# Teoría del Cambio

Una “Teoría del Cambio” es una descripción estructurada de cómo se producirá el cambio esperado (e.g. que el alumnado aprenda) gracias a las actividades puestas en marcha (Rogers, 2014). Se trata por tanto, de una explicación narrativa de por qué el las actividades conseguirán los resultados previstos.Por ejemplo, una teoría del cambio podría ser la siguiente:

>En los centros de adultos mayores se incrementará el número de lectores aficionados a la lectura al poner en marcha clubs de lectura. Esto se producirá porque los usuarios quieren mantener o aumentar el número y calidad de las interacciones sociales, y esto será posible si conocen el argumento de los libros a debate.


La teoría del cambio (TdC) facilita el seguimiento real de los efectos del proyecto y sobre todo, permite comprender cómo funciona el proyecto y cómo se logran los resultados. Por tanto, una teoría del cambio bien planteada ayuda a planificar el proyecto de manera lógica, basándose en las evidencias. También permite identificar qué elementos qué evaluar y cuándo evaluarlo.

La TdC debe hacer referencia a los supuestos sobre los que se basa el cambio previsto. Estos supuestos se pueden redactar como parte de la descripción de cambio en algún apartado posterior aclaratorio, dependiendo de la extensión y complejidad de la narración. Mientras más compleja, la extensión será mayor, y por tanto el lector agradecerá que esté organizada en apartados.

Otro aspecto importante, es la vinculación entre la TdC y los modelos teóricos. La TdC se puede entender como un modelo que representa cómo se genera un programa educativo, estableciendo la relación causal entre el programa y los cambios registrados (Cartwright & Hardie, 2012). Es decir, cuando se dispone de una TdC redactada, se dispone del modelo teórico sobre el que se basa la acción educativa. En este sentido, la TdC es el pilar fundamental del diseño curricular puesto que es el modelo de referencia que puede explicar el éxito o fracaso de la acción educativa.

Por supuesto, para que esto sea posible, la TdC exige un proceso de reflexión continua sobre cómo y por qué suceden los cambios dentro de un determinado contexto educativo (James, 2011). Queda entonces claro, que la TdC, que surge de la evidencia científica, es resultado del esfuerzo para concretar y adaptar dicho conocimiento a la demanda de una realidad concreta (e.g. aula, asignatura, escuela, empresa, región, etc.) que pasa por la reflexión y el análisis crítico.

Si tenemos todo esto en cuenta, esto es: conocimiento basado en evidencias, reflexión crítica, y conocimiento del entorno; podría decirse con toda seguridad, que la TdC también es un reflejo de las teorías implícitas del docente. Por tanto, al analizar una TdC deben tenerse en cuenta los principios científicos en los que se basa, el contexto donde se aplica, así como el proceso cognitivo del docente para darle forma, lo que permitirá una evaluación de la actividad docente desde la realidad del docente, y por tanto, contextualizada en un espacio y momento concreto.


