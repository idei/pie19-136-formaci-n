# Cadena causal o marco lógico
Un modelo lógico es un forma de representar la historia de un proyecto en un diagrama con algunas palabras simples. Muestra la conexión causal entre la necesidad, lo que hace y el resultado. Es una buena manera de visualizar la teoría del cambio y explicar claramente los vínculos entre la teoría, los insumos, las salidas y los resultados. También ayuda a comprender cómo su acción puede contribuir a resultados a más largo plazo o de mayor nivel.

Un modelo lógico ayuda a identificar lo que se espera que suceda y cuándo se espera que suceda. Por lo tanto, proporciona una hoja de ruta para monitorizar el progreso.

El modelo lógico sirve como punto de partida para debatir en torno a la valoración del progreso del proyecto y si ha logrado o no los resultados previstos.

Entre el marco lógico y la teoría del cambio hay relación, aunque mantienen ciertas diferencias. Una forma de explicar la diferencia entre los dos es entender la teoría del cambio como el razonamiento o el pensamiento detrás de lo que se hace, mientras que el modelo lógico es la representación de ese pensamiento en forma de diagrama. El marco lógico es la representación gráfica de la cadena causal, o dicho de otra forma, una cadena causal representada gráficamente, se llama marco lógico.

Existen muchas formas de representación gráfica, que van desde lo más simple hasta las representaciones más complejas. El siguiente ejemplo está adaptado de la Fundación Kellogg (ilustración 2). Para cada proyecto, se puede elegir el tipo de gráfico que mejor se adapte a la complejidad.

![Ilustración 2. Cadena causal](/images/Formacion_PIE_19_21_html_ca2fcf8648f64b5.jpg)
