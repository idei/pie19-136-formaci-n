# Introduction

Este texto se corresponde con el módulo de formación del PIE 136 sobre evaluación de la docencia. Incluye un primer apartado que explica qué es la teoría del cambio dentro del enfoque basado en evidencias. Posteriormente hay un ejemplo desarrollando la primera parte de una evaluación de una programación docente de una asignatura.

El texto incluye enlaces a una explicación en vídeo con una presentación, alojada en un servidor externo.
