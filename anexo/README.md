# Anexo

En el documento titulado "Quality in Qualitative Evaluation: A framework for assessing research evidence" (Spencer, Ritchie, Lewis & Dillon, 2003) presentan una serie de indicadores a tener en cuenta para valorar la calidad de la evaluación independientemente del método utilizado. Estos indicadores se pueden resumir en los siguiente apartados (Research Quality in Publications, s. f.):



**Hallazgos e interpretación**

• ¿Los hallazgos se basan en la evidencia presentada, o hacen inferencias basadas en material u opinión externa? Si es así, ¿están claramente establecido?

• ¿Las conclusiones / hallazgos tienen sentido y tienen una lógica coherente?

• ¿La investigación explica los conceptos y las variables clave que emplea?

• ¿La investigación aborda los fines y objetivos establecidos en el resumen o redefinidos durante el estudio?

• ¿Cuál es el alcance para extraer inferencias más amplias de los hallazgos y qué tan bien se explica esto?

• ¿Hay alguna razón para pensar que la evidencia presentada aquí puede ser no aplicable en otros contextos o puede no haber sido establecida a grupos particulares de personas? (por ejemplo, urbano / rural; hombres / mujeres).

• ¿Hay alguna razón para pensar que los factores contextuales u otros factores pueden haber cambiado desde que se realizó la investigación?

• ¿La investigación identifica las implicaciones para la política y la práctica?


**Diseño de investigación, muestreo y recolección de datos.**

• ¿Todos los elementos del diseño de la investigación, incluidos los métodos, ayudan a cumplir los objetivos del estudio?

• ¿Se utilizarán los métodos de investigación para las preguntas que se hacen?

• ¿Qué tan apropiado es el diseño de la muestra o la selección de casos?

• ¿Se logró a cabo los métodos correctamente? ¿Hay información sobre cómo se implementan los enfoques? (por ejemplo, tasas de respuesta para encuestas, información de muestreo; copia de cuestionarios / preguntas utilizadas).


**Análisis**

• ¿Se ha realizado un análisis exhaustivo, adecuado y preciso? (por ejemplo, nivel de análisis, tratamiento de datos faltantes, tergiversación de muestras / subgrupos pequeños, buen uso de tablas, figuras y cuadros / gráficos, etc.).

• ¿Se consideran los problemas desde una variedad de perspectivas?


**Informes**

• ¿Hasta que punto el informe claro y coherente?

• ¿Existe un vínculo claro entre los datos, la interpretación y las conclusiones?

• ¿Se resaltan y resumen los mensajes clave?

• ¿Se presentan los resultados de la investigación en un formato apropiado para el público?


**Reflexión sobre la investigación**

• ¿Se hacen explícitos los supuestos, las perspectivas teóricas y los valores que han conformado la investigación?

• ¿Hay evidencia de apertura a formas nuevas y alternativas de ver el tema?

• ¿Hay conciencia de las limitaciones del enfoque, los métodos y la evidencia?


**Ética y acceso**

• ¿Se cumplieron las pautas éticas precisas? (por ejemplo, confidencialidad, anonimato, consentimiento informado)

• ¿Hay evidencia de sensibilidad al contexto de investigación ya los participantes?

• ¿La investigación hizo provisiones para permitir la participación de todas las partes relevantes?

• ¿Los investigadores tienen suficiente nivel y tipo de experiencia para emprender la investigación?


**Auditoría**

• ¿Ha sido bien documentado el proceso de investigación?

• ¿Existe documentación y discusión sobre los cambios en el diseño de la investigación?

• ¿Se incluyen los principales documentos de estudio?








