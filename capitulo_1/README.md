# Cómo se elabora una TdC

Una teoría del cambio debe realizarse a partir de las aportaciones de diversos puntos de vista basadas en evidencias. Pueden tenerse en cuenta los comentarios bien fundamentados de los beneficiarios, la experiencia del personal y los voluntarios, evaluaciones de trabajos anteriores, buenas prácticas identificadas por otros que realizan un trabajo similar, e investigaciones publicadas. La construcción de una teoría del cambio se puede resumir en cinco pasos ([NESTA](https://media.nesta.org.uk/documents/theory_of_change_guidance_for_applicants_.pdf)):

* Identificar las metas o resultados últimos.
* Identificar los resultados intermedios.
* Identificar / establecer las actividades del proyecto.
* Mostrar la conexión causal entre actividades y resultados.
* Revisar los supuestos básicos.


Las siguientes preguntas se pueden utilizar para comprobar que se dispone de una buena teoría del cambio:

* ¿Es plausible? ¿Se extrae de fuentes creíbles, reflejando la base de evidencia publicada?
* ¿Es factible? ¿Puede ser llevada a la práctica
* ¿Es comprobable? ¿Se puede evaluar?


La teoría del cambio asume "el cambio" basándose en los supuestos asumidos, que son básicamente teorías implícitas con suficiente apoyo empírico. Al analizar cómo se produce el cambio y su alcance, se pueden plantear las siguientes cuestiones para reflexionar:

* ¿Hasta dónde llega el cambio
* ¿Cómo podemos describir el cambio que se genera desde una visión del aprendizaje?
* ¿Qué tipo de cambios podría indicarnos que el impacto puede prolongarse y ampliarse?


Debe tenerse en cuenta que el cambio es resultados de un proceso no lineal, aunque la teoría del cambio pueda dar esa sensación. Hasta cierto punto es resultados de la interacción caótica entre realidades (sujetos, contextos, acciones, etc.). El impacto de un proyecto se transmite desde el entorno más cercano al más lejano. Así, primero afecta al ámbito de los agentes que forman parte del desarrollo del proyecto y de los beneficiarios. Posteriormente, el efecto puede transmitirse al entorno cercano de estos agentes y beneficiarios. En un siguiente momento, si el proyecto ha tenido suficiente efecto, se puede transmitir al contexto local o regional, y de ahí al nacional o supranacional. Es evidente, que dicha influencia deja de estar bajo control de los que llevan a cabo el programa, desde que su efecto sale del ámbito de los beneficiarios. A partir de ahí, dichos efectos son indirectos (Montague, Young & Montague, 2003) tal como se muestra en la ilustración 1.

* Efectos directos: son resultado inicial del proyecto y están bajo el control del mismo;
* Efectos indirectos: son efectos que no están bajo el control del proyecto, pudiendo ser tanto positivos como negativos.

![Ilustración 1. Ámbitos de influencia](/images/Formacion_PIE_19_21_html_b307a23b753fd457.png)

