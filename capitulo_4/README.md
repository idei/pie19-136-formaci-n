# Ejemplo
Este ejemplo se basa en el análisis que se hizo de la asignatura del grado de Educación Primaria “Observación sistemática y análisis del contextos”. El plan inicial de evaluación de esta asignatura se presentó en un simposium en el congreso de la asociación AIDIPE en 2019. El texto que se presenta a continuación es parte del publicado en el libro de comunicaciones de dicho congreso.

Inicialmente el problema que se plantea es el de analizar si el programa de la asignatura “Observación sistemática y análisis del contextos” que se lleva a cabo en la Universidad de Málaga (UMA) consigue los objetivos propuestos en la misma. Para ello es necesario conocer cuales son los objetivos de la asignatura. Aquí surge el primer punto débil de la programación, puesto que no se definen objetivos del programa. No obstante, si se presenta un listado de competencias que se asumen que deben desarrollarse a lo largo de la asignatura. En la tabla 1 se presentan sólo las competencias específicas para no extender innecesariamente el texto.


Tabla 1. Competencias específicas de la asignatura en la programación de la UMA

| Código | Definición de la competencia |
|--|--|
| 3.6.1. | Comprender que la observación sistemática es un instrumento básico para poder reflexionar sobre la práctica y la realidad, así como contribuir a la innovación y a la mejora en educación infantil.|
| 3.6.2. | Dominar las técnicas de observación y registro|
| 3.6.3. | Abordar análisis de campo mediante metodología observacional utilizando tecnologías de la información, documentación y audiovisuales|
| 3.6.4. | Saber analizar los datos obtenidos, comprender críticamente la realidad y elaborar un informe de conclusiones.|


*Teoría del cambio (TdC)*

Esta es la parte fundamental del proceso en AC. La TdC de la asignatura en la UMA se puede describir tal como sigue:


>Esta asignatura tiene como objetivo que el alumnado adquiera y/o desarrolle las competencias que se indican en la programación docente. Para ello se cuenta con 45 horas a lo largo de 15 semanas, en un aula con mesas móviles, conexión a internet, 1 docente, pizarra tradicional y digital, y video proyector donde desarrollar un temario de 6 temas. Se realizan exposiciones magistrales al inicio de cada tema, para que el alumnado entienda los conceptos. Le siguen prácticas individuales para que se afiance. El alumnado debe estudiar el contenido antes de la siguiente semana donde se realizan actividades grupales para desarrollar la competencias colaborativas. La evaluación se basa de tres tipos de recursos: prácticas de clase, prueba final escrita y a través de un trabajo final en formato informe de investigación. El peso de cada recurso se negocia con el alumnado dos meses antes de la conclusión del curso. Se pretende la participación del alumnado en la decisión de cómo evaluar su aprendizaje.

![Ilustración 3. Cadena causal. Elaboración propia](/images/Formacion_PIE_19_21_html_209e90025e85812b.png)


La TdC se suele representar de manera gráfica.En esta ocasión se ha elegido la cadena causal con la intención de mostrar de forma visual y sintética, los elementos principales de la misma (ilustración 3).
En la ilustración se han incluido algunos de los supuestos en los que se basa la programación. En la medida que fuese posible, estos supuestos deberían basarse en evidencias científicas que deberían referenciarse (en este caso lo dejamos sin hacer para no alargar el ejemplo):

* Los materiales y tiempo son suficientes para dar el contenido eficazmente.
* La clase magistral inicial evita malos entendidos iniciales.
* Las actividades individuales permiten asimilar el contenido.
* Las prácticas grupales fomentan el trabajo colaborativo.
* Debatir sobre la propia evaluación permite definir sistemas más adaptados.
* Las actividades en su conjunto permiten desarrollar las competencias de la asignatura.


Hay una serie de riesgos tales que la TdC podría no representar lo que sucede en la realidad. Estos riesgos están asociados a los supuestos asumidos principalmente. Algunos de estos riesgos son los siguientes:

* El tiempo disponible no es suficiente para obtener los resultados.
* El alumnado no puede acceder al contenido de manera eficaz.
* El tipo de evaluación del alumnado no se ajusta a las características del mismo.
* Las actividades que se llevan a cabo en la asignatura no provocan el cambio previsto.
* Existen otras asignaturas que están dando el mismo contenido de forma más efectiva.
* El alumnado está recibiendo estimulación externa que está aumentando su formación en el contenido propio de la asignatura.


Llegada esta fase, se está en disposición de planificar el proceso de recogida de datos, información y evidencias que apoye la plausibilidad de la TdC. Para ello se definirían las preguntas clave que permitan focalizar el esfuerzo de la evaluación:

* ¿Se ha tenido tiempo suficiente para llevar a cabo el programa?
* ¿El alumnado ha tenido acceso a todo el contenido?
* ¿Las clases magistrales y actividades prácticas han sido eficientes?
* ¿Se ha llevado a cabo el programa según el plan previsto?
* ¿Se ha sentido el alumnado partícipe de la asignatura?
* ¿Se han alcanzado los objetivos competenciales?
* ¿Es este programa más eficiente que otros?
* ¿Otros factores pueden explicar el cambio registrado? ¿se da el contenido en otras asignaturas?


Durante esta fase, el esfuerzo va destinado a recoger datos que permitan contestar a estas preguntas, así como a comprobar la plausibilidad de la teoría del cambio. Las preguntas que se proponen para esta comunicación, los instrumentos y estrategias de recogida de datos para la asignatura, así como os indicadores y las fuentes de información se exponen en la tabla 2.

**Tabla 2.** Estrategias de recogida de datos para apoyar la plausibilidad de la TdC

|Preguntas clave|Instrumento|Indicador/criterio|Fuente|
|---|---|---|---|
|¿Se ha tenido tiempo suficiente para llevar a cabo el programa?|Registro documental|Número de temas dados|Docente|
|¿El alumnado ha tenido acceso a todo el contenido?|Registro documental; Demandas del alumnado| Número de alumnos que accedieron a los contenidos. Número de demandas| Alumnado|
|¿Las clases magistrales y actividades prácticas han sido eficientes?|Cuestionario|Opinión en una escala de apreciación|Alumnado|
|¿Se ha llevado a cabo el programa según el plan previsto?|Registro documental|Número de actividades con relación al calendario|Docente|
|¿Se ha sentido el alumnado partícipe de la asignatura?|Cuestionario|Opinión en una escala de apreciación|Alumnado|
|¿Se han alcanzado los objetivos competenciales?|Examen y pruebas evaluativas|Calificación|Alumnado|
|¿Es este programa más eficiente que otros?|Grupo focal|Grado de acuerdo|Docentes y representantes del alumnado
|¿Otros factores pueden explicar el cambio registrado? ¿se da el contenido en otras asignaturas?|Grupo focal|Grado de acuerdo|Docentes y representanes del alumnado|


Fuente: Elaboración propia


Las siguientes fases son propias de un proceso de investigación socio crítico o dirigido al cambio. A partir de los datos recopilados el docente va analizando la realidad a la luz de las conclusiones que se obtienen. La idea es buscar nueva información que vaya en contra de las conclusiones, de forma que se pueda incrementar la validez de los resultados a través de la falsación. Este procedimiento de saturación debería completarse con otro de triangulación de fuentes en la medida de lo posible.



**Referencias** **
Cartwright, N. and J. Hardie (2012). *Evidence-based Policy: Doing It Better. A Practical Guide to Predicting If a Policy Will Work for You*. Oxford: Oxford University Press.


James, C. (2011). *Theory of change review. A report commissioned by Comic Relief*. Disponible en https://www.actknowledge.org/resources/documents/James_ToC.pdf


Petty, G. (2014). *Evidence-Based Teaching. A Practical Approach*. Oxford, U.K.: Oxford University Press. ENLACE


Rogers, P. (2014). *Theory of Change, Methodological Briefs: Impact Evaluation 2*. Florencia: UNICEF Office of Research.

